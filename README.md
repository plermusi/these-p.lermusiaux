# Manuscrit de thèse :
## <a href="https://gitlab.inria.fr/plermusi/these-p.lermusiaux/-/raw/master/manuscript.pdf" download>Télécharger le manuscrit</a>
# Article LOPSTR 2020 :
Introduction de la notion d'Exemption de Motif et du formalisme de sémantique de motif. Présentation de la méthode d'analyse de CBTRS par vérification des post-conditions.
## <a href="https://gitlab.inria.fr/plermusi/these-p.lermusiaux/-/raw/master/lopstr_2020.pdf" download>Télécharger l'article de LOPSTR 2020</a>
# Article PPDP 2021 :
Introduction du système d'annotation par profil. Présentation de la méthode d'analyse de CBTRS avec inférence sur les pré-conditions des profils. Première approche non-linéaire par confluence.
## <a href="https://gitlab.inria.fr/plermusi/these-p.lermusiaux/-/raw/master/ppdp_2021.pdf" download>Télécharger l'article de PPDP 2021</a>
