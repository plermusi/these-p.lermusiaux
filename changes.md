<details> <summary> 27/06 </summary>

## Correction du lien vers le dépôt de l'implémentation

</details>

<details> <summary> 30/06 </summary>

## Changement de la notation de motif annotant (et équivalent)

## Correction d'une confusion entre le Lemme 4.2 et la Proposition 4.1

## Correction de quelque typos

</details>

<details> <summary> 04/07 </summary>

## Simplification de la preuve de la Proposition 3.1

</details>

<details> <summary> 06/07 </summary>

## Description de l'argument de contradiction de la preuve de la Proposition 3.1

</details>

<details> <summary> 29/07 </summary>

## Ajout d'une remarque sur la notation des tuples p.10

## Ajout de l'Exemple 1.2 sur la linéarité des motifs étendus

## Ajout de la Définition 1.25 sur la Complétude d'un CBTRS
(Les numéros des définitions suivantes du Chapitre 1 sont donc décalés)

## Ajout de l'Exemple 3.6 sur l'Exemption de Motif
(Les numéros des exemples suivants du Chapitre 3 sont donc décalés)

Reprend les cas d'exemption de l'Exemple 3.9 (anciennement 3.8) mais sans la sémantique

## Changement de la preuve de la Proposition 3.3
### meilleure distinction entre la preuve de l'implication directe et indirecte
### contraposée plus explicite pour la preuve de l'implication directe
### retrait d'une contraposée non-nécessaire dans la preuve de l'implication indirecte
### insistance sur l'intérêt de la substitution vers une valeur dans la preuve de l'impliation directe
### ajout d'une note en pied de page explicitant que l'implication directe est donnée par les Propositions 3.6 et 3.7 en utilisant la sémantique

## Ajout d'un paragraphe p.50 explicitant l'utilisation de nouveaux opérateurs de conjonction et d'aliasing

## Rappel de la notation des tuples en pied de la p.67

## Ajout de quelques arguments sur la terminaison du calcul de point fixe p.68

## Ajout de quelques détails sur l'induction utilisée dans l'Exemple 4.3

## Changement de la Section 4.5
### Ajout d'une sous-section avec un Théorème explicitant les hypothèses et le résultat de l'analyse linéaire
### Récapitulatif des trois étapes d'analyse linéaire

## Ajout de quelque détails sur la notion de faux-négatif avant et dans l'Exemple 5.2

## Ajout d'une note en pied de la p.105 rappelant la notation de la relation de réécriture stricte

## Changement de la Section 5.4
### Ajout d'une sous-section avec un Théorème explicitant les hypothèses et le résultat de l'analyse non-linéaire
### L'Exemple 6.2 a été déplacé à la suite (maintenant Exemple 5.14) pour détailler l'intérêt des hypothèses de confluence et de terminaison (failble).
### Récapitulatif des deux étapes d'analyse non-linéaire

## Quelque petits changement dans les tables du Chapitre 6 et leurs légendes

## Ajout de quelque détails sur la conclusion de l'Exemple 6.1

## Changement de la présentation des complexités pour mettre en avant les paramètres de nombre de règles et de profils

## Renommage de l'approche "par défaut" en approche "agile"
(Déjà utilisée auparavant mais pas universellement)

## Changement de la Section 6.3
### Ajout d'arguments sur les hypothèses de terminaison, confluence et complétude
### Mention de la notion de parité comme exemple de propriété ne pouvant être exprimée par l'Exemption de Motif (cf. Exemple 2.3)
### Ajout de l'Exemple 6.4 comparant l'expression du propriété par Exemption de Motif et via un automate
### Ajout de l'Exemple 6.5 explicitant l'encodage d'un système en PMRS

## Ajout de détails sur le changement des cas insertion sort, merge sort, delete et sorted delete pour vérifier la préservation de la sémantique avec l'approche par linéarisation

## Quelque changements de mise en forme et corrections de typo

</details>
